from component.sqlite_core import SQLiteCore


class SQLite(SQLiteCore):
    
    def __init__(self, db_location):
        self.db_location = db_location
    
    
    
    def select_user_film_rating(self, user_id, movie_id):
        
        query = 'SELECT rating FROM ratings WHERE userId = {} AND movieId = {}'.format(user_id, movie_id)
        result = self.select(query)[0][0]
        return result
    
        